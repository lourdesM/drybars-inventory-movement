﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//using System.Web.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Xml.Xsl;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
namespace DryBarInventoryMovement
{
   public class Program
    {
        private static string SQL_CONNECTION_STRING = "server=54.149.142.14;database=RN_THEDRYBAR_SETUP;user id=sa; Password=H9mhbr2k";// System.Configuration.ConfigurationManager.ConnectionStrings["DataAccessQuickStart"].ConnectionString;
       //SANDBOX URL
        //private static string netSuiteURL_InventoryMovement = "https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=234&deploy=1";
       //PRODUCTION URL
        private static string netSuiteURL_InventoryMovement = "https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=215&deploy=1";
       
       private static string netSuiteData_InventoryMovement = "";
        private static string LogFilePath = "~/InventoryMovementLogs";
        private static DateTime _lastInventoryLog;
        private static string _logInventoryFeedsToNetSuites;
        public class netSuiteResult
        {
            public string status { get; set; }
            public string message { get; set; }
            public string intergationId { get; set; }
            public string locationId { get; set; }
            public string UpdateType { get; set; }
        }

        public class dataImportResult
        {
            public string status { get; set; }
            public string message { get; set; }
            public string locationId { get; set; }
            public string UpdateType { get; set; }
        }
        static void Main()
        {
            InventoryMovement();
        }
        private static void InventoryMovement()
        {
            var postEntries = true;
            var totalcount = 0;
            var countPosted = 0;
            var countNotPosted = 0;
             dataImportResult dataImportResult = new dataImportResult();
             dataImportResult.status="Success";
             List<dataImportResult> dataImportFailed = new List<dataImportResult>();
             DataSet dsItemDetails1 = new DataSet();

            try
            {

               var StartDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
               var EndDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                
                netSuiteResult result = new netSuiteResult();
                List<netSuiteResult> resultFailed = new List<netSuiteResult>();
              
              
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();

                SqlDataAdapter distinctScom = new SqlDataAdapter("SELECT DISTINCT LocationID, ClosedOrderDate, UpdateType FROM fxnInventoryMovements('" + StartDate + "', '" + EndDate + "') ORDER BY ClosedOrderDate, LocationID", adocn);
                distinctScom.SelectCommand.CommandTimeout = 0;
                DataSet dsDisctinctInventoryMovement = new DataSet();
                distinctScom.Fill(dsDisctinctInventoryMovement);
                //int test = 0;
                if (dsDisctinctInventoryMovement.Tables[0].Rows.Count > 0)
                {
              
                    foreach (DataRow dr in dsDisctinctInventoryMovement.Tables[0].Rows)
                    {
                        totalcount++;
                       
                        netSuiteData_InventoryMovement = "{";
                        netSuiteData_InventoryMovement += "\"dateOfUpdate\": \"" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "\", ";
                        netSuiteData_InventoryMovement += "\"customerId\": \"" + dr["LocationID"] + "\", ";
                        netSuiteData_InventoryMovement += "\"updateType\": \"" + dr["UpdateType"] + "\", ";
                        netSuiteData_InventoryMovement += "\"itemDetail\": [";

                        var sqlStr = "";
                        if (dr["LocationID"].ToString() == "")
                        {
                            sqlStr = "SELECT * FROM fxnInventoryMovements('" + StartDate + "', '" + EndDate + "') WHERE LocationID IS NULL AND UpdateType='" + dr["UpdateType"] + "' AND ClosedOrderDate='" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "'";
                        }
                        else
                        {
                            sqlStr = "SELECT * FROM fxnInventoryMovements('" + StartDate + "', '" + EndDate + "') WHERE LocationID='" + dr["LocationID"] + "' AND UpdateType='" + dr["UpdateType"] + "' AND ClosedOrderDate='" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "'";
                        }
                        //** var sqlStr = "SELECT * FROM fxnInventoryMovements('" + StartDate + "', '" + EndDate + "') WHERE LocationID='" + dr["LocationID"] + "'  AND UpdateType='" + dr["UpdateType"] + "' AND ClosedOrderDate='" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "' ";
                        SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
                        scom.SelectCommand.CommandTimeout = 0;
                        DataSet dsItemDetails = new DataSet();
                        dsItemDetails1 = dsItemDetails;
                        scom.Fill(dsItemDetails);

                        if (dsItemDetails.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr1 in dsItemDetails.Tables[0].Rows)
                            {

                                 netSuiteData_InventoryMovement += "{";
                                 netSuiteData_InventoryMovement += "\"qtySoldOrConsumed\": \"" + dr1["Quantity"] + "\", ";
                                 netSuiteData_InventoryMovement += "\"skuId\": \"" + dr1["SKU"] + "\", ";
                                 netSuiteData_InventoryMovement += "}, ";


                                 //****CHECK IF ENTRIES HAVE ALREADY BEEN POSTED************
                                 var sqlStr3 = "";
                                 dr1["ItemName"] = dr1["ItemName"].ToString().Replace("'", "''");
                                 if (dr1["LocationID"].ToString() == "")
                                 {
                                     sqlStr3 = "SELECT Posted_IntegrationId FROM [dbo].[InventoryMovementPosted] WHERE LocationID IS NULL AND Quantity=" + dr1["Quantity"] + " AND ItemName='" + dr1["ItemName"] + "' AND ClosedOrderDate='" + dr1["ClosedOrderDate"] + "' AND SKU='" + dr1["SKU"] + "' AND UpdateType='" + dr1["UpdateType"] + "' AND IsDeleted=0";
                                 }
                                 else
                                 {
                                     sqlStr3 = "SELECT Posted_IntegrationId FROM [dbo].[InventoryMovementPosted] WHERE LocationID=" + dr1["LocationID"] + " AND Quantity=" + dr1["Quantity"] + " AND ItemName='" + dr1["ItemName"] + "' AND ClosedOrderDate='" + dr1["ClosedOrderDate"] + "' AND SKU='" + dr1["SKU"] + "' AND UpdateType='" + dr1["UpdateType"] + "' AND IsDeleted=0";
                                 }
                                 SqlDataAdapter scom1 = new SqlDataAdapter(sqlStr3, adocn);
                                 scom1.SelectCommand.CommandTimeout = 0;
                                 DataSet dsItemPosted = new DataSet();
                                 scom1.Fill(dsItemPosted);
                                 for (int i = 0; i < dsItemPosted.Tables[0].Rows.Count; i++)
                                 {
                                     if (dsItemPosted.Tables[0].Rows[i]["Posted_IntegrationId"].ToString() != "")
                                     {
                                         postEntries = false;
                                     }
                                 }
                                //*****************************************************

                            }
                            netSuiteData_InventoryMovement += "]}";

                            if (postEntries)
                            {
                                countPosted++;
                                result = postInventoriesToNetStuites(dsItemDetails);
                                result.locationId = dr["LocationID"].ToString();
                                result.UpdateType = dr["UpdateType"].ToString();
                                if (result.status != "Success")
                                {
                                    resultFailed.Add(result);
                                }
                            }
                            else
                            {
                                countNotPosted++;
                            }
                           //** result = postInventoriesToNetStuites(dsItemDetails);
                           //** result.locationId = dr["LocationID"].ToString();
                           //** result.UpdateType= dr["UpdateType"].ToString();
                           /*** if (result.status != "Success")
                            {
                                resultFailed.Add(result);
                            }*/
                        }
                        else
                        {
                            dataImportResult.status = "Failed";
                            dataImportResult.message = "No Inventory Movement Files Found.";
                            dataImportResult.locationId=dr["LocationID"].ToString();
                            dataImportFailed.Add(dataImportResult);
                        }                    

                    }
                  

                    if (dataImportFailed.Count() > 0)
                    {
                        string strMsg = "No Inventory Movement Files found for the following Location Internal IDs: \r\n \r\n";
                            foreach(var i in dataImportFailed) {
                                strMsg += i.locationId + "\r\n";
                            }
                            SendingEmailNotification(strMsg);
                    }


                    if (resultFailed.Count() > 0)
                    {
                        string strMsg = "The following error was encountered while posting the Inventory Movement Files to NetSuite : \r\n \r\n";
                        strMsg += "<table>";
                        strMsg += "<tr><td style='font-weight:bold'>LocationInternalID</td><td style='font-weight:bold'>Update Type</td><td style='font-weight:bold'>Error Message</td></tr>";
                        
                        foreach (var i in resultFailed)
                        {
                            //strMsg += "LocationInternalID: " + i.locationId + " UpdateType: "+ i.UpdateType+ " - " + i.message + " \r\n";
                            strMsg += "<tr><td>" + i.locationId + "</td><td>" + i.UpdateType + "</td><td>" + i.message + "</td></tr>";
                            
                        }
                        strMsg += "</table>";
                        SendingEmailNotification(strMsg);
                    }

                }
                else
                {
                    

                     string strMsg = "No Inventory Movement Data found for Sales Date "+ StartDate +".";
                     SendingEmailNotification(strMsg);
                }

                adocn.Close();
             
                

            }
            catch (Exception e)
            {
              

                string strMsg = "An error occurred while processing the Inventory Movement. Please contact your System Administrator.";
               
                SendingEmailNotification(strMsg);
                
            }

          
          
        }
        private static void InitializeInventoryMovementToNetSuitesLog()
        {
            var path = GetLogFilePath();
            path = Path.Combine(path, "InventoryMovementLogs_" + DateTime.Now.ToString("yyyyMMdd-HHMMss") + ".txt");
            System.IO.File.AppendAllText(path, _logInventoryFeedsToNetSuites);
            if (System.IO.File.Exists(path))
            {
                try
                {
                    _lastInventoryLog = DateTime.Parse(System.IO.File.ReadAllText(path));
                }
                catch { }
            }

        }
        private static string GetLogFilePath()
        {
            var path = System.Web.Hosting.HostingEnvironment.MapPath(LogFilePath);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path = Path.Combine(path, "InventoryMovementLogs");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
        private static netSuiteResult postInventoriesToNetStuites(DataSet dsItemDetails)
        {
            netSuiteResult NSResult = new netSuiteResult();
            try
            {
                var storeResponse = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(netSuiteURL_InventoryMovement);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "*/*";

                String requestParams = "nlauth_account=4116791, nlauth_email=datamart.integration@thedrybar.com, nlauth_signature=Drybar@999, nlauth_role=1022";
                request.Headers.Add("Authorization: NLAuth " + requestParams);

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = netSuiteData_InventoryMovement;

                    var serializer = new JavaScriptSerializer();

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (Stream streamReader = httpResponse.GetResponseStream())
                    {
                        using (StreamReader r = new StreamReader(streamReader))
                        {
                            var result = r.ReadToEnd();
                            //string status = "";
                            //string resultMessage = "";
                            //string integrationId = "";
                            JavaScriptSerializer j = new JavaScriptSerializer();
                            netSuiteResult a = (netSuiteResult)j.Deserialize(result, typeof(netSuiteResult));
                           

                            NSResult.status = a.status;
                            NSResult.message = a.message;
                            if (a.status == "Success")
                            {
                                NSResult.intergationId = a.intergationId;
                            }
                            else
                            {
                                NSResult.intergationId = "";
                            }

                            storeResponse = storeDataToInventoryMovementPostedTable(dsItemDetails, NSResult.intergationId, NSResult.status + " : " + NSResult.message);
                            return NSResult;
                        }
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        //Console.WriteLine(text);
                        NSResult.status = "Failed";
                        NSResult.message = text;
                        NSResult.intergationId = "";
                        return NSResult;
                    }
                }
            }
        }
        private static string storeDataToInventoryMovementPostedTable(DataSet dsItemDetails, string integrationId, string status)
        {
            try
            {
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();
                foreach (DataRow dr in dsItemDetails.Tables[0].Rows)
                {
                    string sql = "INSERT INTO [dbo].[InventoryMovementPosted] (LocationID, Quantity, ItemName, ClosedOrderDate, SKU, UpdateType, Posted_date, Posted_status, Posted_IntegrationId) " +
                                                      "VALUES (@LocationID, @Quantity, @ItemName, @ClosedOrderDate, @SKU, @UpdateType, @Posted_date, @Posted_status, @Posted_IntegrationId)";
                    SqlCommand command = new SqlCommand(sql, adocn);
                    command.Parameters.AddWithValue("@LocationID", (dr["LocationID"] != null && dr["LocationID"] != "") ? dr["LocationID"] : null);
                    command.Parameters.AddWithValue("@Quantity", dr["Quantity"]);
                    command.Parameters.AddWithValue("@ItemName", dr["ItemName"]);
                    command.Parameters.AddWithValue("@ClosedOrderDate", dr["ClosedOrderDate"]);
                    command.Parameters.AddWithValue("@SKU", dr["SKU"]);
                    command.Parameters.AddWithValue("@UpdateType", dr["UpdateType"]);
                    command.Parameters.AddWithValue("@Posted_date", DateTime.Today);
                    command.Parameters.AddWithValue("@Posted_status", status);
                    command.Parameters.AddWithValue("@Posted_IntegrationId", integrationId);

                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();

                }
                adocn.Close();
                return "Success";
            }
            catch (SqlException e)
            {
                return "Failed";
            }
        }

        protected static void SendingEmailNotification(string Message)
        {
                 MailMessage mail1 = new MailMessage();
                    SmtpClient SmtpServer1 = new SmtpClient("smtp.gmail.com", 587);

                    mail1.From = new MailAddress("somidata@gmail.com", "Inventory Movement");
                    mail1.To.Add("bonnie@thedrybar.com");
                    mail1.To.Add("steve@thedrybar.com");
                    mail1.To.Add("justin@somidata.com");
                    mail1.To.Add("juliana@somidata.com");
                    mail1.To.Add("lourdes@somidata.com");
                    mail1.Subject = "Error: Inventory Movement Data for Sales Date " + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");

                    //string strMessageBody1 = Message;
                    mail1.Body = Message;


                    mail1.IsBodyHtml = true;
                   // SmtpServer1.Port = 587;
                    SmtpServer1.Credentials = new System.Net.NetworkCredential("somidata@gmail.com", "S0miData$");
                    SmtpServer1.EnableSsl = true;

                    SmtpServer1.Send(mail1);
        }
    }
}
